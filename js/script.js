(function() {
  var requestAnimationFrame =
    window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.msRequestAnimationFrame;
  window.requestAnimationFrame = requestAnimationFrame;
})();

var lFollowX = 0,
  lFollowY = 0,
  x = 0,
  y = 0,
  friction = 1 / 100;

function moveBackground() {
  x += (lFollowX - x) * friction;
  y += (lFollowY - y) * friction;

  // console.log(x, y);

  translate = "translate(" + x + "px, " + y + "px) scale(1)";
  if (mobilecheck()) {
    translate = "translate(" + x + "px, " + y + "px) scale(0.5)";
  }
  translate2 = "translate(" + x * 0.15 + "px, " + y * 0.15 + "px) scale(1)";
  translate3 = "translate(" + x * 0.1 + "px, " + y * 0.1 + "px) scale(1)";
  translate4 = "translate(" + x * 0.15 + "px, " + y * 0.15 + "px) scale(1)";
  translate5 = "translate(" + x * 0.08 + "px, " + y * 0.08 + "px) scale(1)";
  translate6 = "translate(" + x * 0.3 + "px, " + y * 0.3 + "px) scale(1)";

  $("#bgMove").css({
    "-webit-transform": translate,
    "-moz-transform": translate,
    transform: translate
  });

  $("#proj1").css({
    "-webit-transform": translate6,
    "-moz-transform": translate6,
    transform: translate6
  });

  $("#proj2").css({
    "-webit-transform": translate3,
    "-moz-transform": translate3,
    transform: translate3
  });

  $("#proj3").css({
    "-webit-transform": translate4,
    "-moz-transform": translate4,
    transform: translate4
  });

  $("#proj4").css({
    "-webit-transform": translate2,
    "-moz-transform": translate2,
    transform: translate2
  });

  $("#proj5").css({
    "-webit-transform": translate3,
    "-moz-transform": translate3,
    transform: translate3
  });

  $("#proj6").css({
    "-webit-transform": translate6,
    "-moz-transform": translate6,
    transform: translate6
  });

  $("#proj7").css({
    "-webit-transform": translate3,
    "-moz-transform": translate3,
    transform: translate3
  });

  $("#proj8").css({
    "-webit-transform": translate3,
    "-moz-transform": translate3,
    transform: translate3
  });

  $("#proj9").css({
    "-webit-transform": translate3,
    "-moz-transform": translate3,
    transform: translate3
  });

  $("#proj10").css({
    "-webit-transform": translate3,
    "-moz-transform": translate3,
    transform: translate3
  });

  $("#proj11").css({
    "-webit-transform": translate6,
    "-moz-transform": translate6,
    transform: translate6
  });

  $("#proj12").css({
    "-webit-transform": translate5,
    "-moz-transform": translate5,
    transform: translate5
  });

  window.requestAnimationFrame(moveBackground);
}

$(window).on("mousemove click", function(e) {
  var lMouseX = Math.max(
    -100,
    Math.min(100, $(window).width() / 2 - e.clientX)
  );
  var lMouseY = Math.max(
    -100,
    Math.min(100, $(window).height() / 2 - e.clientY)
  );
  lFollowX = (1000 * lMouseX) / 150 - 500; // 100 : 12 = lMouxeX : lFollow
  lFollowY = (1000 * lMouseY) / 370 - 150;
});

moveBackground();
function jggRules() {
  if ($("body").hasClass("home")) {
    initMenu();
  }
  setSizes();
  lateralParallax();
  videoHoverHome();
  if (mobilecheck()) {
    insertControls();
  } else {
    addVideoPlayListeners();
  }
}
function mobilecheck() {
  var check = false;
  (function(a) {
    if (
      /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
        a
      ) ||
      /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
        a.substr(0, 4)
      )
    )
      check = true;
  })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
}
function initMenu() {
  console.log("initMenu");
  if (mobilecheck()) {
    $("#Menu").addClass("closed");
    $("#btn-menu").text("MENÚ  + ");
  } else {
    $("#Menu").removeClass("closed");
    $("#btn-menu").text("MENÚ  - ");
  }
  $("#btn-menu").click(function() {
    if ($("#Menu").hasClass("closed")) {
      window.requestAnimationFrame(function() {
        $("#btn-menu").text("MENÚ  - ");
        $("#Menu").removeClass("closed");
      });
    } else {
      window.requestAnimationFrame(function() {
        $("#Menu").addClass("closed");
        $("#btn-menu").text("MENÚ  + ");
      });
    }
  });
}
function addVideoPlayListeners() {
  $("video.clicktoplay").each(function(index) {
    $(this).on("click touchstart", function() {
      console.log("clicked");
      if ($(this)[0].paused) {
        var promise = $(this)[0].play();
        if (promise !== undefined) {
          promise
            .then(_ => {
              // Autoplay started!
            })
            .catch(error => {
              // Autoplay was prevented.
              // Show a "Play" button so that user can start playback.
            });
        } else {
          // alert('No promise')
        }
      } else {
        $(this)[0].pause();
      }
    });
  });
}
function setSizes() {
  $(".two-images").each(function(index) {
    // console.log($(this).children().outerWidth())
    $(this).css(
      "width",
      $(this)
        .children()
        .outerWidth() + "px"
    );
  });
  $(".three-images").each(function(index) {
    // console.log($(this).children().outerWidth())
    $(this).css(
      "width",
      $(this)
        .children()
        .outerWidth() + "px"
    );
  });
  $(".three-row").each(function(index) {
    // console.log($(this).children().outerWidth())
    $(this).css(
      "width",
      $(this)
        .find("video")
        .outerWidth() *
        3 +
        "px"
    );
  });
}
function insertControls() {
  $(".clicktoplay").each(function(index) {
    $(this).attr("controls", true);
  });
}
function playVideos() {
  // console.log('hey')
  $("video.vautoplay").each(function(index) {
    var promise = $(this)[0].play();
    if (promise !== undefined) {
      promise
        .then(_ => {
          // Autoplay started!
        })
        .catch(error => {
          // Autoplay was prevented.
          // Show a "Play" button so that user can start playback.
        });
    } else {
      // console.log("no promise");
    }
  });
}
function lateralParallax() {
  var speed = 40;
  var lastScrollTop = 0;
  totalWidth = 0;
  $(".projCont")
    .children()
    .each(function() {
      totalWidth = totalWidth + $(this).width();
    });
  // console.log(totalWidth)
  $(".projCont").scroll(function(e) {
    var scrollNum = $(".projCont").scrollLeft();
    scrollPercent =
      $(".projCont").scrollLeft() / (totalWidth - $(window).width());

    // console.log(scrollPercent);
    $("#px1").css(
      "transform",
      "translateX(" + -300 * (scrollPercent - 0.79) + "px)"
    );
    $("#px2").css(
      "transform",
      "translateX(" + -900 * (scrollPercent - 0.81) + "px)"
    );
    $("#px3").css(
      "transform",
      "translateX(" + -1500 * (scrollPercent - 0.89) + "px)"
    );
    $("#px4").css(
      "transform",
      "translateX(" + -600 * (scrollPercent - 0.96) + "px)"
    );

    $("#px1_11130").css(
      "transform",
      "translateX(" + 300 * (scrollPercent - 0.56) + "px)"
    );
    $("#px2_11130").css(
      "transform",
      "translateX(" + -300 * (scrollPercent - 0.56) + "px)"
    );
    $("#px3_11130").css(
      "transform",
      "translateX(" + -1500 * (scrollPercent - 0.6) + "px)"
    );
    $("#px4_11130").css(
      "transform",
      "translateX(" + -3000 * (scrollPercent - 0.64) + "px)"
    );
    $("#px5_11130").css(
      "transform",
      "translateX(" + -1800 * (scrollPercent - 0.64) + "px)"
    );
    $("#px1_carbon").css(
      "transform",
      "translateX(" + 2400 * (scrollPercent - 0.9) + "px)"
    );

    lastScrollTop = scrollNum;
  });
}
function videoHoverHome() {
  setSizes();
  if ($("body").hasClass("home")) {
    $("video").each(function(index) {
      $(this).hover(function() {
        //console.log("hover");
        $(this).removeAttr("muted");
      });
    });
  }
}
var fakeMouseX = 0;
var fakeMouseY = 0;
/*
var polarX = 0;
var polarY = 0;
*/
var midX = $(document).width() / 2;
var midY = $(document).height() / 2;

$(window).on("load", function() {
  jggRules();
  // if ($("body").hasClass("home") && !mobilecheck()) {

    // if(!mobilecheck()){
    //   $('#btn-menu').css('display', 'none');
    //   // $('#Menu').css('top','60px');
    // }

  if ($("body").hasClass("home") && !mobilecheck()) {
    perspectiveTransform(1);
    perspectiveTransform(2);
    perspectiveTransform(3);
    perspectiveTransform(4);
    perspectiveTransform(5);
    perspectiveTransform(6);
    perspectiveTransform(7);
    perspectiveTransform(8);
    perspectiveTransform(9);
    perspectiveTransform(10);
    perspectiveTransform(11);
    perspectiveTransform(12);
    perspectiveTransform(13);
  }
  if ($("body").hasClass("home") && mobilecheck()) {
    /*
    if (!!window.IntersectionObserver) {
      const myImgs = document.querySelectorAll(".proj");
      var options = {
        threshold: 0.66
      };
      observer = new IntersectionObserver((entry, observer) => {
        // console.log("entry:", entry[0]);
        for (var i = 0; i < entry.length; i++) {
          //console.log("is?:", entry[i].isIntersecting);
          //console.log(entry[i].target.querySelector("video"));
          //entry[i].target.querySelector("video").style.transform =
          ("rotate3d(1,0,0,1deg)");
          if (entry[i].isIntersecting) {
            var eH =
              entry[i].target.offsetTop + entry[i].target.offsetHeight / 2;
            var eW =
              entry[i].target.offsetLeft + entry[i].target.offsetWidth / 2;
            var polarx = ($(document).width() - eW) / $(document).width();
            var polary = ($(document).height() - eH) / $(document).height();
            if (entry[i].target.getElementsByTagName("video").length > 0) {
              entry[i].target.getElementsByTagName("video")[0].style.transform =
                "rotate3d(" + -1 * polary + "," + polarx + ",0,1deg)";
              entry[i].target.style.opacity = 0.9;
            }
            if (entry[i].target.getElementsByTagName("img").length > 0) {
              entry[i].target.getElementsByTagName("img")[0].style.transform =
                "rotate3d(" + -1 * polary + "," + polarx + ",0,1deg)";
              entry[i].target.style.opacity = 0.9;
            }
          } else {
            if (entry[i].target.getElementsByTagName("video").length > 0) {
              entry[i].target.getElementsByTagName("video")[0].style.transform =
                "";
              entry[i].target.style.opacity = 0.5;
            }
            if (entry[i].target.getElementsByTagName("img").length > 0) {
              entry[i].target.getElementsByTagName("img")[0].style.transform =
                "";
              entry[i].target.style.opacity = 0.5;
            }
          }
        }
      }, options);
      myImgs.forEach(image => {
        observer.observe(image);
      });
    } else {
      console.log("NOT IO");
    }
    */
  }
});
$(document).ready(function() {
  $("#Title").hover(
    function() {
      // console.log('some');
      // $( "#secCont").css('opacity', 1);
    },
    function() {
      // $( "#secCont").css('opacity', 0);
    }
  );

  var onSec = false;


  if(!$('#secProjCont').length)
    onSec = true;
  else
    onSec = false;
  


   $('html, body, *').mousewheel(function(e, delta) {

      if(onSec){
        this.scrollLeft -= (delta);
        e.preventDefault();
      }


    });

});




// ===========================================================
// See tutorial at :
// https://css-tricks.com/animate-a-container-on-mouse-over-using-perspective-and-transform/
// ===========================================================

function perspectiveTransform(index) {
  // Init
  var container = document.getElementById("proj" + index),
    inner = document.getElementById("vid" + index);

  // Mouse
  var mouse = {
    _x: 0,
    _y: 0,
    x: 0,
    y: 0,
    updatePosition: function(event) {
      var e = event || window.event;
      this.x = e.clientX - this._x;
      this.y = (e.clientY - this._y) * -1;
    },
    setOrigin: function(e) {
      this._x = e.offsetLeft + Math.floor(e.offsetWidth / 2);
      this._y = e.offsetTop + Math.floor(e.offsetHeight / 2);
    },
    show: function() {
      return "(" + this.x + ", " + this.y + ")";
    }
  };

  //   // Track the mouse position relative to the center of the container.
  mouse.setOrigin(container);

  //   //----------------------------------------------------

  var counter = 0;
  var refreshRate = 10;
  var isTimeToUpdate = function() {
    return counter++ % refreshRate === 0;
  };

  //   //----------------------------------------------------

  var onMouseEnterHandler = function(event) {
    update(event);
  };

  var onMouseLeaveHandler = function() {
    inner.style = "";
  };

  var onMouseMoveHandler = function(event) {
    // console.log("MOUSE!!!");
    if (isTimeToUpdate()) {
      update(event);
    }
  };
  //----------------------------------------------------

  var update = function(event) {
    mouse.updatePosition(event);
    updateTransformStyle(
      (mouse.y / inner.offsetHeight / 2).toFixed(2),
      (mouse.x / inner.offsetWidth / 2).toFixed(2)
    );
  };

  var updateTransformStyle = function(x, y) {
    var style = "rotateX(" + x + "deg) rotateY(" + y + "deg)";
    inner.style.transform = style;
    inner.style.webkitTransform = style;
    inner.style.mozTranform = style;
    inner.style.msTransform = style;
    inner.style.oTransform = style;
  };

  //   //--------------------------------------------------------

  container.onmousemove = onMouseMoveHandler;
  container.onmouseleave = onMouseLeaveHandler;
  container.onmouseenter = onMouseEnterHandler;
}
