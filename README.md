# Portfolio para 11130 Studio

Portfolio para la marca visual 11130 Studio. Se trata de una landing en la que el scroll se realiza mediante el hover que haga el usuario a través de toda la pantalla mientras se muestran los proyectos. El contenido de estos se muestran en forma horizontal. Se emplea GreenSock y diversas animaciones de JQuery.

http://11130gvngestudio.com/
